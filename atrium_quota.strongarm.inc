<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_quota_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_limit';
  $strongarm->value = '2';
  $export['user_limit'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_limit_active';
  $strongarm->value = 0;
  $export['user_limit_active'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_limit_message';
  $strongarm->value = 'Your user limit has been reached. To add more users, please contact your service provider.';
  $export['user_limit_message'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_limit_uid1';
  $strongarm->value = 0;
  $export['user_limit_uid1'] = $strongarm;

  return $export;
}
